// https://votecharlie.com/blog/2018/08/improved-color-wheel-function.html
// Adapted from https://www.stm32duino.com/viewtopic.php?t=56#p8160

unsigned int sqrt32(unsigned long n) {
    unsigned int c = 0x8000;
    unsigned int g = 0x8000;
    while(true) {
    if(g*g > n) {
        g ^= c;
    }
    c >>= 1;
    if(c == 0) {
        return g;
    }
    g |= c;
    }
}

// Input values 0 to 255 to get color values that transition R->G->B. 0 and 255
// are the same color. This is based on Adafruit's Wheel() function, which used
// a linear map that resulted in brightness peaks at 0, 85 and 170. This version
// uses a quadratic map to make values approach 255 faster while leaving full
// red or green or blue untouched. For example, Wheel(42) is halfway between
// red and green. The linear function yielded (126, 129, 0), but this one yields
// (219, 221, 0). This function is based on the equation the circle centered at
// (255,0) with radius 255:  (x-255)^2 + (y-0)^2 = r^2
Color Wheel(uint8_t position) {
    uint8_t R = 0, G = 0, B = 0;
    if (position < 85) {
    R = sqrt32((1530 - 9 * position) * position);
    G = sqrt32(65025 - 9 * position * position);
    } else if (position < 170) {
    position -= 85;
    R = sqrt32(65025 - 9 * position * position);
    B = sqrt32((1530 - 9 * position) * position);
    } else {
    position -= 170;
    G = sqrt32((1530 - 9 * position) * position);
    B = sqrt32(65025 - 9 * position * position);
    }
    return Color(R, G, B);
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
Color WheelAda(uint8_t WheelPos) {
    WheelPos = 255 - WheelPos;
    if(WheelPos < 85) {
        return Color(255 - WheelPos * 3, 0, WheelPos * 3);
    }
    if(WheelPos < 170) {
        WheelPos -= 85;
        return Color(0, WheelPos * 3, 255 - WheelPos * 3);
    }
    WheelPos -= 170;
    return Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}

// See Appendix on https://docs.arduino.cc/language-reference/en/functions/math/map/
long map(long x, long in_min, long in_max, long out_min, long out_max) {
      return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }


