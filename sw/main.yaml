# Inspired by

substitutions:
  device: !secret device
  version: "0.26.0"
  led_pin: GPIO25
  sda_pin: GPIO21
  scl_pin: GPIO22
  dallas_pin: GPIO23
  rf_pin: GPIO36
  led_count: "72"
  response_buffer_size: "8192"

esphome:
  name: ${device}
  comment: Mapa CR s daty z TMEP.cz
  project:
    name: "dstancl.mapa_cr"
    version: $version
  includes:
    - wheel.h
  on_boot:
    priority: -100
    then:
      - script.execute: getTemperaturesData
      - script.execute: getPrecipitationData
      - script.execute: getPrecipitationDataPes

esp32:
  board: esp32doit-devkit-v1
  framework:
    type: esp-idf

wifi:
  ssid: !secret wifi_ssid
  password: !secret wifi_password

# HomeAssistant API
api:

# Enable web server
web_server:
  port: 80

# Logs
logger:
  level: INFO

# OTA
ota:
  platform: esphome

safe_mode:

# I2C
i2c:
  - id: bus_a
    sda: $sda_pin
    scl: $scl_pin
    scan: True

# DS18B20 (internal temperature)
one_wire:
  - platform: gpio
    pin: ${dallas_pin}
    
# Global variables
globals:
  - id: tempData
    type: String
    restore_value: false
    initial_value: '"{}"'
  - id: precipitationData
    type: String
    restore_value: false
    initial_value: '"{}"'
  - id: precipitationDataPes
    type: String
    restore_value: false
    initial_value: '"{}"'
  - id: tempMinMax
    type: Color[${led_count}]
    restore_value: no
  - id: tempMinMaxInitialized
    type: bool
    restore_value: no
    initial_value: 'false'
  - id: tempAbs
    type: Color[${led_count}]
    restore_value: no
  - id: tempAbsInitialized
    type: bool
    restore_value: no
    initial_value: 'false'
  - id: precipitationTMEP
    type: Color[${led_count}]
    restore_value: no
  - id: precipitationTMEPInitialized
    type: bool
    restore_value: no
    initial_value: 'false'
  - id: precipitationPes
    type: Color[${led_count}]
    restore_value: no
  - id: precipitationPesInitialized
    type: bool
    restore_value: no
    initial_value: 'false'
  - id: tempMin
    type: float
    restore_value: no
    initial_value: '100.0'
  - id: tempMax
    type: float
    restore_value: no
    initial_value: '-100.0'

# Debug
debug:

# Light (LEDs)
light:
  - platform: esp32_rmt_led_strip
    rgb_order: GRB
    chipset: WS2812
    pin: ${led_pin}
    num_leds: ${led_count}
    rmt_channel: 0
    name: "${device} LEDs"
    id: neo
    restore_mode: RESTORE_DEFAULT_OFF
    default_transition_length:
      seconds: 0
    effects:
      - addressable_color_wipe:
      - addressable_color_wipe:
          name: "Color Wipe Effect With Custom Values"
          colors:
            - red: 100%
              green: 100%
              blue: 100%
              num_leds: 1
            - red: 0%
              green: 0%
              blue: 0%
              num_leds: 1
          add_led_interval: 100ms
          reverse: false
      - addressable_lambda:
          name: "Teploty z TMEP.cz minmax"
          update_interval: 3000ms
          lambda: |-
            bool initialized = id(tempMinMaxInitialized);
            auto a = id(tempMinMax);
            Color black = Color(0, 0, 0);
            for (int i = 0; i < ${led_count}; i++)
            {
              it[i] = initialized ? a[i] : black;
            } // for i
      - addressable_lambda:
          name: "Teploty z TMEP.cz primo"
          update_interval: 3000ms
          lambda: |-
            bool initialized = id(tempAbsInitialized);
            auto a = id(tempAbs);
            Color black = Color(0, 0, 0);
            for (int i = 0; i < ${led_count}; i++)
            {
              it[i] = initialized ? a[i] : black;
            } // for i
      - addressable_lambda:
          name: "Srazky z TMEP.cz"
          update_interval: 3000ms
          lambda: |-
            bool initialized = id(precipitationTMEPInitialized);
            auto a = id(precipitationTMEP);
            Color black = Color(0, 0, 0);
            for (int i = 0; i < ${led_count}; i++)
            {
              it[i] = initialized ? a[i] : black;
            } // for i
      - addressable_lambda:
          name: "Srazky od J. Cizka"
          update_interval: 3000ms
          lambda: |-
            bool initialized = id(precipitationPesInitialized);
            auto a = id(precipitationPes);
            Color black = Color(0, 0, 0);
            for (int i = 0; i < ${led_count}; i++)
            {
              it[i] = initialized ? a[i] : black;
            } // for i
      - addressable_lambda:
          name: "Color test"
          update_interval: 3000ms
          lambda: |-
            for (int i = 0; i < ${led_count}; i++)
            {
              int color = map(i, 0, ${led_count}, 170, 0);
              Color c = WheelAda(color);
              it[i] = c;
            } // for i

json:

http_request:
  id: http_request_data
  useragent: esphome/rgbcrmap
  timeout: 10s

time:
  - platform: sntp
    id: time_service
    on_time:
      # Every 10 minutes get temperature data
      # starts on 2 (sequence is 2, 12, 22 etc) to allow
      # update TMEP.cz JSON data
      - seconds: 0
        minutes: 2/10
        then:
          - script.execute: getTemperaturesData
          - script.execute: getPrecipitationData
          - script.execute: getPrecipitationDataPes

wireguard:
  address: !secret wireguard_ip
  private_key: !secret wireguard_key_private
  peer_public_key: !secret wireguard_key_server
  peer_endpoint: !secret wireguard_endpoint
  peer_port: !secret wireguard_port
  peer_persistent_keepalive: 25s

script:
  # Get temperatures
  - id: getTemperaturesData
    mode: queued
    then:
      - logger.log:
          level: info
          format: "HTTP Request get Data"
      - http_request.get:
          url: "http://cdn.tmep.cz/app/export/okresy-cr-teplota.json"
          capture_response: true
          max_response_buffer_size: ${response_buffer_size}
          headers:
            Content-Type: application/json
          on_response:
            - if:
                condition:
                  lambda: |-
                    return response->status_code == 200; // = OK
                then:
                  - logger.log:
                      level: info
                      tag: "getTemperaturesData"
                      format: "Response status: %d, Duration: %u ms, Response length: %d byte(s)"
                      args:
                        - response->status_code
                        - response->duration_ms
                        - response->content_length
                  - lambda: |-
                      id(tempData) = body;
                  - text_sensor.template.publish:
                      id: last_update_temperature
                      state: !lambda |
                        return id(time_service).now().strftime("%Y-%m-%d %H:%M:%S");
                  - script.execute: setTempMinMax
                  - script.execute: setTempAbs

  # Get precipitation (TMEP.cz)
  - id: getPrecipitationData
    mode: queued
    then:
      - logger.log:
          level: info
          format: "HTTP Request get precipitation data (TMEP.cz)"
      - http_request.get:
          url: "http://tmep.cz/app/export/okresy-srazky-laskakit.json"
          capture_response: true
          max_response_buffer_size: ${response_buffer_size}
          headers:
            Content-Type: application/json
          on_response:
            - if:
                condition:
                  lambda: |-
                    return response->status_code == 200; // = OK
                then:
                  - logger.log:
                      level: info
                      tag: "getPrecipitationData"
                      format: "Response status: %d, Duration: %u ms"
                      args:
                        - response->status_code
                        - response->duration_ms
                  - lambda: |-
                      id(precipitationData) = body;
                  - text_sensor.template.publish:
                      id: last_update_precipitation
                      state: !lambda |
                        return id(time_service).now().strftime("%Y-%m-%d %H:%M:%S");
                  - script.execute: setPrecipitationTMEP

  # Get precipitation (J. Cizek, @pesvklobouku)
  - id: getPrecipitationDataPes
    mode: queued
    then:
      - logger.log:
          level: info
          format: "HTTP Request get precipitation data (@pesvklobouku)"
      - http_request.get:
          url: "http://oracle-ams.kloboukuv.cloud/radarmapa/?chcu=posledni.json"
          capture_response: true
          max_response_buffer_size: ${response_buffer_size}
          headers:
            Content-Type: application/json
          on_response:
            - if:
                condition:
                  lambda: |-
                    return response->status_code == 200; // = OK
                then:
                  - logger.log:
                      level: info
                      tag: "getPrecipitationData"
                      format: "Response status: %d, Duration: %u ms"
                      args:
                        - response->status_code
                        - response->duration_ms
                  - lambda: |-
                      id(precipitationDataPes) = body;
                  - text_sensor.template.publish:
                      id: last_update_precipitation_pes
                      state: !lambda |
                        return id(time_service).now().strftime("%Y-%m-%d %H:%M:%S");
                  - script.execute: setPrecipitationPes

  - id: setTempMinMax
    mode: queued
    then:
      - lambda: |-
          DynamicJsonDocument doc(6144);
          ESP_LOGD("json", "length: %d, content: %s", id(tempData).length(), id(tempData).c_str());
          DeserializationError error = deserializeJson(doc, id(tempData));
          if (error)
          {
            ESP_LOGE("json", "deserializeJson() failed: %s", error.c_str());
            return;
          }
          float TMEPDistrictTemperatures[${led_count}];
 
          double tMin = 100, tMax = -100; 
          // Find min and max temperature
          for (JsonObject item : doc.as<JsonArray>())
          {
            int TMEPdistrictIndex = item["id"];
            TMEPdistrictIndex -= 1;
            double h = item["h"];
            if (TMEPdistrictIndex < ${led_count})
               TMEPDistrictTemperatures[TMEPdistrictIndex] = h;
            if (h < tMin) tMin = h;
            if (h > tMax) tMax = h;
          } // for item
          ESP_LOGD("display", "minTemp: %f, maxTemp: %f\n", id(tempMin), id(tempMax));
          // Setup display array (tempMinMax)
          auto tempMinMaxArray = id(tempMinMax);
          for (int led = 0; led < ${led_count}; led++)
          {
            float h = TMEPDistrictTemperatures[led];
            int color = map(h, tMin, tMax, 170, 0);
            // int color = map(led, 0, ${led_count}, 170, 0);  // DEBUG
            Color c = WheelAda(color);
            tempMinMaxArray[led] = c;
          } // for led
          id(tempMinMaxInitialized) = true;
          // Set sensors
          id(tempMin) = tMin;
          id(tempMax) = tMax;
          id(temperature_min).publish_state(id(tempMin));
          id(temperature_max).publish_state(id(tempMax));
  - id: setTempAbs
    mode: queued
    then:
      - lambda: |-
          DynamicJsonDocument doc(6144);
          ESP_LOGD("json", id(tempData).c_str());
          DeserializationError error = deserializeJson(doc, id(tempData));
          if (error)
          {
            ESP_LOGE("json", "deserializeJson() failed: %s", error.c_str());
            return;
          }
          float maxTemp = 40;
          float minTemp = -10;
          float TMEPDistrictTemperatures[${led_count}];

          // Find min and max temperature
          for (JsonObject item : doc.as<JsonArray>())
          {
            int TMEPdistrictIndex = item["id"];
            TMEPdistrictIndex -= 1;
            double h = item["h"];
            if (TMEPdistrictIndex < ${led_count})
              TMEPDistrictTemperatures[TMEPdistrictIndex] = h;
            //if (h < minTemp) minTemp = h;
            //if (h > maxTemp) maxTemp = h;
          } // for item
          ESP_LOGD("display", "minTemp: %f, maxTemp: %f\n", minTemp, maxTemp);

          // Display
          auto tempAbsArray = id(tempAbs);
          for (int led = 0; led < ${led_count}; led++)
          {
            float h = TMEPDistrictTemperatures[led];
            int color = map(h, minTemp, maxTemp, 170, 0);
            // int color = map(led, 0, ${led_count}, 170, 0);  // DEBUG
            tempAbsArray[led] = WheelAda(color);
          } // for led
          id(tempAbsInitialized) = true;
  - id: setPrecipitationTMEP
    mode: queued
    then:
      - lambda: |-
          DynamicJsonDocument doc(6144);
          ESP_LOGD("json", id(precipitationData).c_str());
          DeserializationError error = deserializeJson(doc, id(precipitationData));
          if (error)
          {
            ESP_LOGE("json", "deserializeJson() failed: %s", error.c_str());
            return;
          }
            
          // Display
          Color black = Color(0, 0, 0);
          auto dataArray = id(precipitationTMEP);
          for (int ledID = 0; ledID < ${led_count}; ledID++)
          {
            dataArray[ledID] = black;
          }
          for (JsonObject city : doc["seznam"].as<JsonArray>())
          {
            int cityId = city["id"],
              r = city["r"],
              g = city["g"],
              b = city["b"];
            Color c = Color(r, g, b);
            dataArray[cityId-1] = c;
           //ESP_LOGD("display", "okres: %d, barva rgb: %d, %d, %d\n", cityId, r, g, b);
          } // for city
          id(precipitationTMEPInitialized) = true;
  - id: setPrecipitationPes
    mode: queued
    then:
      - lambda: |-
          DynamicJsonDocument doc(6144);
          ESP_LOGD("json", id(precipitationDataPes).c_str());
          DeserializationError error = deserializeJson(doc, id(precipitationDataPes));
          if (error)
          {
            ESP_LOGE("json", "deserializeJson() failed: %s", error.c_str());
            return;
          }
          
          // Display
          Color black = Color(0, 0, 0);
          auto dataArray = id(precipitationPes);
          for (int ledID = 0; ledID < ${led_count}; ledID++)
          {
            dataArray[ledID] = black;
          }
          for (JsonObject city : doc["seznam"].as<JsonArray>())
          {
            int cityId = city["id"],
              r = city["r"],
              g = city["g"],
              b = city["b"];
            Color c = Color(r, g, b);
            dataArray[cityId-1] = c;

            //ESP_LOGD("display", "okres: %d, barva rgb: %d, %d, %d\n", cityId, r, g, b);
          } // for city
          id(precipitationPesInitialized) = true;

button:
  - platform: restart
    name: "${device} restart"

  - platform: template
    name: "${device} reload data"
    on_press:
      - then:
        - logger.log: Reload data invoked
        - script.execute: getTemperaturesData
        - script.execute: getPrecipitationData
        - script.execute: getPrecipitationDataPes

sensor:
  # Wi-Fi signal
  - platform: wifi_signal
    name: "${device} signal"
    entity_category: diagnostic
    update_interval: 60s
  # ESP32 internal temperature sensor
  - platform: internal_temperature
    name: "${device} chip temperature"
    unit_of_measurement: °C
    accuracy_decimals: 1
    device_class: temperature
    entity_category: diagnostic
    icon: mdi:chip
  # uptime - seconds, human readable
  - platform: uptime
    name: "${device} uptime_sec"
    id: uptime_sensor
    update_interval: 60s
    entity_category: diagnostic
    on_raw_value:
      then:
        - text_sensor.template.publish:
            id: uptime_human
            state: !lambda |-
              int seconds = round(id(uptime_sensor).raw_state);
              int days = seconds / (24 * 3600);
              seconds = seconds % (24 * 3600);
              int hours = seconds / 3600;
              seconds = seconds % 3600;
              int minutes = seconds /  60;
              seconds = seconds % 60;
              char buffer[20];
              sprintf(buffer, "%dT%02d:%02d:%02d", days, hours, minutes, seconds);
              return buffer;
 # Debug (memory info)
  - platform: debug
    free:
      name: "${device} heap free"
      entity_category: diagnostic
    block:
      name: "${device} heap max block"
      entity_category: diagnostic
    loop_time:
      name: "${device} loop time"
      entity_category: diagnostic
  # Wireguard
  - platform: wireguard
    latest_handshake:
      name: "${device} WireGuard last handshake"
      entity_category: diagnostic
  # Dallas DS18B20 (on board)
  - platform: dallas_temp
    name: "${device} board temperature"
    address: !secret dallas_address_board
    entity_category: diagnostic
  # Minimal temperature
  - platform: template
    name: "${device} min temperature"
    id: temperature_min
    device_class: temperature
    unit_of_measurement: °C
    accuracy_decimals: 1
  # Maximal temperature
  - platform: template
    name: "${device} max temperature"
    id: temperature_max
    device_class: temperature
  # Photoresistor (night level)
  - platform: adc
    id: night_level_raw
    unit_of_measurement: V
    accuracy_decimals: 2
    pin: ${rf_pin}
    attenuation: 12db
    filters:
      # Convert to range 0 - 1 V
      - multiply: 0.303
      # Set high level to 1.0
      - clamp:
          max_value: 1.0
  # Night level in percent
  - platform: template
    id: light_level
    name: "${device} Light Level Percent"
    accuracy_decimals: 0
    unit_of_measurement: "%"
    icon: mdi:white-balance-sunny
    lambda: |-
      return (1.0-id(night_level_raw).state)*100;
  # Temperature from homeassistant (outdoor temperature)
  - platform: homeassistant
    id: temperature_ha_outdoor
    entity_id: !secret sensor_temperature_outdoor

text_sensor:
  # Network info
  - platform: wifi_info
    ip_address:
      name: "${device} IP Address"
      entity_category: diagnostic
      icon: mdi:ip-network
    ssid:
      name: "${device} Connected SSID"
      entity_category: diagnostic
    bssid:
      name: "${device} Connected BSSID"
      entity_category: diagnostic
      disabled_by_default: true
    mac_address:
      name: "${device} MAC WiFi Address"
      entity_category: diagnostic
      disabled_by_default: true
    scan_results:
      name: "${device} Latest Scan Result"
      entity_category: diagnostic
      disabled_by_default: true
  # Uptime
  - platform: template
    name: "${device} uptime"
    id: uptime_human
    icon: mdi:clock-start
  # Version
  - platform: template
    name: "${device} version"
    id: template_version
    icon: mdi:new-box
    entity_category: diagnostic
    lambda: |-
      return { "${version}" };
    update_interval: 24hours
  # Debug (device info)
  - platform: debug
    device:
      name: "${device} device info"
      entity_category: diagnostic
    reset_reason:
      name: "${device} reset reason"
      entity_category: diagnostic
  # Last update date and time
  - platform: template
    name: "${device} last update temperature"
    id: last_update_temperature
    icon: mdi:clock-check-outline
  - platform: template
    name: "${device} last update precipitation TMEP"
    id: last_update_precipitation
    icon: mdi:clock-check-outline
  - platform: template
    name: "${device} last update precipitation Cizek"
    id: last_update_precipitation_pes
    icon: mdi:clock-check-outline
  
font:
  - file: "fonts/arial.ttf"
    id: font16
    size: 16
  - file: "fonts/arial.ttf"
    id: font12
    size: 12
  - file: "fonts/arial.ttf"
    id: font8
    size: 8
  - file: "fonts/arial.ttf"
    id: font32
    size: 32

number:
  # Set OLED contrast
  - platform: template
    name: "${device} OLED contrast"
    id: OLED_contrast
    icon: "mdi:contrast-circle"
    disabled_by_default: true
    entity_category: config
    min_value: 0
    max_value: 100
    step: 1
    optimistic: true
    restore_value: true
    on_value:
      then:
        lambda: |-
          if (id(OLED_contrast).state) {
            id(oled_display).set_contrast(x / 100);
          } else {
            id(oled_display).set_contrast(0.5);
          }

interval:
  # Rotate display pages
  - interval: 5s
    then:
      - display.page.show_next: oled_display
      - component.update: oled_display

display:
  - platform: ssd1306_i2c
    model: "SH1106 128x64"
    address: 0x3c
    id: oled_display
    pages:
      - id: page_main
        lambda: |-
          it.print(it.get_width()/2, 0, id(font12), TextAlign::TOP_CENTER, "Mapa CR");
          it.strftime(0, (it.get_height()-16)/2, id(font16), "%H:%M, %d.%m.%Y", id(time_service).now());
          it.print(0, it.get_height()-12, id(font12), id(neo).get_effect_name().c_str());
      - id: page_outdoor
        lambda: |-
          it.print(it.get_width()/2, 0, id(font12), TextAlign::TOP_CENTER, "Teplota venku");
          it.printf(it.get_width()-1, (it.get_height()-32)/2+1, id(font32), TextAlign::TOP_RIGHT, "%.1f °C", id(temperature_ha_outdoor).state);
